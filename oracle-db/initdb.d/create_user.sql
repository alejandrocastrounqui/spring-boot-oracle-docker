ALTER session SET "_ORACLE_SCRIPT"=true;

create tablespace api_tablespace
  logging
  datafile '/home/oracle/api_tablespace_01.dbf' 
  size 32m 
  autoextend on 
  next 32m maxsize 2048m
  extent management local;

create temporary tablespace api_temporal 
  tempfile '/home/oracle/api_temporal_01.dbf' 
  size 32m 
  autoextend on 
  next 32m maxsize 2048m
  extent management local;

CREATE USER api_admin
  IDENTIFIED BY api_pass
  DEFAULT TABLESPACE api_tablespace
  TEMPORARY TABLESPACE api_temporal
  QUOTA 20M on api_tablespace;
  
GRANT create session TO api_admin;
GRANT create table TO api_admin;
GRANT create view TO api_admin;
GRANT create any trigger TO api_admin;
GRANT create any procedure TO api_admin;
GRANT create sequence TO api_admin;
GRANT create synonym TO api_admin;



--end
