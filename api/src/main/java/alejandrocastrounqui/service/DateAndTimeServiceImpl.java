package alejandrocastrounqui.service;

import java.util.Calendar;

import org.springframework.stereotype.Component;

@Component
public class DateAndTimeServiceImpl implements DateAndTimeService{

    @Override
    public Integer dayOfMonth() {
        Calendar cal = Calendar.getInstance();
        Integer dayOfMonth = cal.get(Calendar.DAY_OF_MONTH);
        return dayOfMonth;
    }

}
