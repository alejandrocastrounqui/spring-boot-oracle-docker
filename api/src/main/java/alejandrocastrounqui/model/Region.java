package alejandrocastrounqui.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="REGIONS")
public class Region {

    @Id
    @Column(name="REGION_ID", columnDefinition="NUMBER")
    protected Integer id;

    @Column(name="REGION_NAME", unique = true, length=25)
    private String name;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
