package alejandrocastrounqui.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Entity
@Table(name="LOCATIONS")
public class Location {

    @Id
    @Column(name="LOCATION_ID", length=4, columnDefinition="NUMBER")
    private Integer id;

    @Column(name="STREET_ADDRESS", length=40)
    private String streetAddress;

    @Column(name="POSTAL_CODE", length=12)
    private String postalCode;

    @Column(name="CITY", length=30)
    private String city;

    @Column(name="STATE_PROVINCE", length=25)
    private String stateProvince;

    @ManyToOne
    @JoinColumn(name="COUNTRY_ID")
    private Country country;

    @OneToMany(mappedBy="location")
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Department> deparments;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStateProvince() {
        return stateProvince;
    }

    public void setStateProvince(String stateProvince) {
        this.stateProvince = stateProvince;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public List<Department> getDeparments() {
        return deparments;
    }

    public void setDeparments(List<Department> deparments) {
        this.deparments = deparments;
    }



}
