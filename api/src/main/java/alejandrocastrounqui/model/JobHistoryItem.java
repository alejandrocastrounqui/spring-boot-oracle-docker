package alejandrocastrounqui.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="JOB_HISTORY")
public class JobHistoryItem implements Serializable{

    private static final long serialVersionUID = 1L;

    @Id
    @ManyToOne
    @JoinColumn(name="EMPLOYEE_ID")
    private Employee employee;

    @Id
    @ManyToOne
    @JoinColumn(name="DEPARTMENT_ID")
    private Department department;

    @Id
    @ManyToOne
    @JoinColumn(name="JOB_ID")
    private Job job;

    @Id
    @Temporal(TemporalType.DATE)
    @Column(name="START_DATE")
    private Date startDate;

    @Id
    @Temporal(TemporalType.DATE)
    @Column(name="END_DATE")
    private Date endDate;

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public Job getJob() {
        return job;
    }

    public void setJob(Job job) {
        this.job = job;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }


}
