package alejandrocastrounqui.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="COUNTRIES")
public class Country {

    @Id
    @Column(name="COUNTRY_ID", length=2, columnDefinition="CHAR")
    private String id;

    @Column(name="COUNTRY_NAME", unique = true, length=40)
    private String name;

    @ManyToOne
    @JoinColumn(name="REGION_ID", columnDefinition="NUMBER")
    private Region region;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }



}
