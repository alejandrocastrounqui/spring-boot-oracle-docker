package alejandrocastrounqui.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.annotations.Parameter;

@Entity
@Table(name="EMPLOYEES")
public class Employee {

    @GenericGenerator(name = "generator", strategy = "sequence-identity", parameters = @Parameter(name = "sequence", value = "api_employees_seq"))
    @Id
    @GeneratedValue(generator = "generator")
    @Column(name="EMPLOYEE_ID", length=4, columnDefinition="NUMBER")
    private Integer id;

    @Column(name="FIRST_NAME", length=20)
    private String firstName;

    @Column(name="LAST_NAME", length=20, nullable=false)
    private String lastName;

    @Column(name="EMAIL", length=25, nullable=false)
    private String email;

    @Column(name="PHONE_NUMBER", length=25)
    private String phoneNumber;

    @Temporal(TemporalType.DATE)
    @Column(name="HIRE_DATE")
    private Date hireDate;

    @Column(name="SALARY", length=10, precision=2, columnDefinition="NUMBER")
    private Double salary;

    @Column(name="COMMISSION_PCT", length=4, precision=2, columnDefinition="NUMBER")
    private Double commisionPCT;

    @ManyToOne
    @JoinColumn(name="MANAGER_ID")
    private Employee manager;

    @ManyToOne
    @JoinColumn(name="DEPARTMENT_ID")
    private Department department;

    @ManyToOne
    @JoinColumn(name="JOB_ID")
    private Job job;

    @OneToMany(mappedBy="employee")
    @LazyCollection(LazyCollectionOption.FALSE)
    List<JobHistoryItem> jobHistory;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Date getHireDate() {
        return hireDate;
    }

    public void setHireDate(Date hireDate) {
        this.hireDate = hireDate;
    }

    public Double getSalary() {
        return salary;
    }

    public void setSalary(Double salary) {
        this.salary = salary;
    }

    public Double getCommisionPCT() {
        return commisionPCT;
    }

    public void setCommisionPCT(Double commisionPCT) {
        this.commisionPCT = commisionPCT;
    }

    public Employee getManager() {
        return manager;
    }

    public void setManager(Employee manager) {
        this.manager = manager;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public Job getJob() {
        return job;
    }

    public void setJob(Job job) {
        this.job = job;
    }

    public List<JobHistoryItem> getJobHistory() {
        return jobHistory;
    }

    public void setJobHistory(List<JobHistoryItem> jobHistory) {
        this.jobHistory = jobHistory;
    }



}
