package alejandrocastrounqui.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="DEPARTMENTS")
public class Department {

    @Id
    @Column(name="DEPARTMENT_ID", length=4, columnDefinition="NUMBER")
    private Integer id;

    @Column(name="DEPARTMENT_NAME", unique = true, length=30, nullable=false)
    private String name;

    @ManyToOne
    @JoinColumn(name="LOCATION_ID", columnDefinition="NUMBER")
    private Location location;

    @ManyToOne
    @JoinColumn(name="MANAGER_ID", columnDefinition="NUMBER")
    private Employee manager;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Employee getManager() {
        return manager;
    }

    public void setManager(Employee manager) {
        this.manager = manager;
    }


}
