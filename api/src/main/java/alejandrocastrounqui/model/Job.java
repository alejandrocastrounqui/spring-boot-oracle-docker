package alejandrocastrounqui.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="JOBS")
public class Job {

    @Id
    @Column(name="JOB_ID", length=10)
    private String id;

    @Column(name="JOB_TITLE", nullable=false, length=35)
    private String title;

    @Column(name="MIN_SALARY", length=6, columnDefinition="NUMBER")
    private Integer minSalary;

    @Column(name="MAX_SALARY", length=6, columnDefinition="NUMBER")
    private Integer maxSalary;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getMinSalary() {
        return minSalary;
    }

    public void setMinSalary(Integer minSalary) {
        this.minSalary = minSalary;
    }

    public Integer getMaxSalary() {
        return maxSalary;
    }

    public void setMaxSalary(Integer maxSalary) {
        this.maxSalary = maxSalary;
    }


}
