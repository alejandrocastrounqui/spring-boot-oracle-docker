package alejandrocastrounqui.exception;

public class UserNotFountException extends CustomRuntimeException{

    private static final long serialVersionUID = 1L;

    public UserNotFountException() {
        super("USER_NOT_FOUND", "");
    }

}
