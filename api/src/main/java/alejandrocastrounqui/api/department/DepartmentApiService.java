package alejandrocastrounqui.api.department;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import alejandrocastrounqui.api.department.dto.DepartmentDTO;
import alejandrocastrounqui.api.department.dto.DepartmentIndexDTO;
import alejandrocastrounqui.api.department.extra.InvalidContextException;
import alejandrocastrounqui.model.Department;
import alejandrocastrounqui.model.Employee;
import alejandrocastrounqui.model.Location;
import alejandrocastrounqui.service.DateAndTimeService;

@Transactional
@Service("departmentApiService")
public class DepartmentApiService {

    private static final Logger LOGGER = LoggerFactory.getLogger(DepartmentApiService.class);

    @Autowired
    DepartmentApiRepository departmentApiRepository;

    @Autowired
    DepartmentSerializer departmentSerializer;

    @Autowired
    DateAndTimeService dateAndTimeService;

    @Transactional
    public void create(DepartmentDTO departmentDTO) {
        LOGGER.info("creating deparment");
        Department department = departmentSerializer.dtoToModel(departmentDTO);
        create(department);
    }

    protected void create(Department department) {
        Location location = department.getLocation();
        validateAverageSalary(location);
        departmentApiRepository.save(department);
    }

    protected void validateAverageSalary(Location location) {
        List<Employee> employeeList = departmentApiRepository.getEmployeesByLocation(location);
        Double salaryAverage;
        //avoid divide by zero
        if(employeeList.isEmpty()) {
            salaryAverage = 0.0;
        }
        else {
            Double salarySum = 0.0;
            for (Employee employee : employeeList) {
                salarySum += employee.getSalary();
            }
            salaryAverage = salarySum / employeeList.size();
        }
        Integer dayOfMonth = dateAndTimeService.dayOfMonth();
        Integer threshold;
        if(dayOfMonth < 15) {
            threshold = 1000;
        }
        else {
            threshold = 1500;
        }
        if(salaryAverage > threshold) {
            throw new InvalidContextException("employee salary average exceed allowed threshold");
        }
    }

    public List<DepartmentIndexDTO> index(){
        List<Department> departmenList = departmentApiRepository.findAll();
        List<DepartmentIndexDTO> departmentIndexDTOList = departmentSerializer.modelToIndexDTO(departmenList);
        return departmentIndexDTOList;
    }

}
