package alejandrocastrounqui.api.department.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class DepartmentDTO {

    @NotNull(message="department-id-not-null")
    private Integer id;

    @NotBlank(message="department-name-not-blank")
    private String name;

    @NotNull(message="department-location-not-null")
    private Integer location;

    private Integer manager;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getLocation() {
        return location;
    }

    public void setLocation(Integer location) {
        this.location = location;
    }

    public Integer getManager() {
        return manager;
    }

    public void setManager(Integer manager) {
        this.manager = manager;
    }

}
