package alejandrocastrounqui.api.department.dto;

public class DepartmentIndexDTO {

    private Integer id;

    private String name;

    private DepartmentIndexLocationDTO location;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DepartmentIndexLocationDTO getLocation() {
        return location;
    }

    public void setLocation(DepartmentIndexLocationDTO location) {
        this.location = location;
    }


}
