package alejandrocastrounqui.api.department;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import alejandrocastrounqui.common.BasicRepository;
import alejandrocastrounqui.model.Department;
import alejandrocastrounqui.model.Employee;
import alejandrocastrounqui.model.Location;

@Repository("departmentApiRepository")
public interface DepartmentApiRepository extends BasicRepository<Department, Integer> {

    @Query("SELECT location " +
            "FROM Location location " +
            "WHERE location.id = :locationId")
    Location getLocation(@Param("locationId") Integer locationId);

    @Query("SELECT employee " +
            "FROM Employee employee " +
            "WHERE employee.id = :employeeId")
    Employee getEmployee(@Param("employeeId") Integer employeeId);

    @Query("SELECT employee " +
            "FROM Employee employee " +
            "JOIN employee.department department " +
            "WHERE department.location = :location")
    List<Employee> getEmployeesByLocation(@Param("location") Location location);
}