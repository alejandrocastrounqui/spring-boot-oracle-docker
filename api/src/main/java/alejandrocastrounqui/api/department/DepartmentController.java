package alejandrocastrounqui.api.department;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import alejandrocastrounqui.api.department.dto.DepartmentDTO;
import alejandrocastrounqui.api.department.dto.DepartmentIndexDTO;

@RestController
@RequestMapping("${rest.base_path}/department")
public class DepartmentController {

    private static final Logger LOGGER = LoggerFactory.getLogger(DepartmentController.class);

    @Autowired
    DepartmentApiService departmentApiService;

    @ResponseBody
    @RequestMapping(value = "", method = RequestMethod.POST)
    public void create(
        @RequestBody
        @Valid
        DepartmentDTO departmentDTO
            ) {
        LOGGER.info("creating new department");
        departmentApiService.create(departmentDTO);
    }


    @ResponseBody
    @RequestMapping(value = "", method = RequestMethod.GET)
    public List<DepartmentIndexDTO> index() {
        LOGGER.info("retriving departments");
        List<DepartmentIndexDTO> departmentIndexDTOList = departmentApiService.index();
        return departmentIndexDTOList;
    }

}
