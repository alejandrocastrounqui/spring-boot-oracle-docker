package alejandrocastrounqui.api.department.extra;

import alejandrocastrounqui.exception.CustomRuntimeException;

public class InvalidContextException extends CustomRuntimeException {

    private static final long serialVersionUID = 1L;

    public InvalidContextException(String message) {
        super("invalid-context", message);
    }


}
