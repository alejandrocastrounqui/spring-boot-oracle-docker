package alejandrocastrounqui.api.department;

import java.util.LinkedList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import alejandrocastrounqui.api.department.dto.DepartmentDTO;
import alejandrocastrounqui.api.department.dto.DepartmentIndexDTO;
import alejandrocastrounqui.model.Department;
import alejandrocastrounqui.model.Employee;
import alejandrocastrounqui.model.Location;

@Component
public class DepartmentSerializer {

    ModelMapper modelMapper = new ModelMapper();

    @Autowired
    DepartmentApiRepository departmentApiRepository;

    public DepartmentDTO modelToDTO(Department department) {
        DepartmentDTO departmentDTO = modelMapper.map(department, DepartmentDTO.class);
        return departmentDTO;
    }

    public Department dtoToModel(DepartmentDTO departmentDTO) {
        Department department = modelMapper.map(departmentDTO, Department.class);
        Integer locationId = departmentDTO.getLocation();
        Location location = departmentApiRepository.getLocation(locationId);
        department.setLocation(location);
        Integer managerId = departmentDTO.getManager();
        Employee manager = departmentApiRepository.getEmployee(managerId);
        department.setManager(manager);
        return department;
    }

    public List<DepartmentIndexDTO> modelToIndexDTO(List<Department> departments) {
        List<DepartmentIndexDTO> result = new LinkedList<DepartmentIndexDTO>();
        for (Department department : departments) {
            DepartmentIndexDTO departmentIndexDTO = modelMapper.map(department, DepartmentIndexDTO.class);
            result.add(departmentIndexDTO);
        }
        return result;
    }

}
