package alejandrocastrounqui.api.job;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import alejandrocastrounqui.api.job.dto.JobDTO;

@RestController
@RequestMapping("${rest.base_path}/job")
public class JobController {

    private static final Logger LOGGER = LoggerFactory.getLogger(JobController.class);

    @Autowired
    JobApiService jobApiService;

    @ResponseBody
    @RequestMapping(value = "", method = RequestMethod.GET)
    public List<JobDTO> index() {
        LOGGER.info("retriving cuentasContables");
        List<JobDTO> jopIndex = jobApiService.index();
        return jopIndex;
    }

}
