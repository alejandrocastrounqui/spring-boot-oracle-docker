package alejandrocastrounqui.api.job;

import java.util.LinkedList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import alejandrocastrounqui.api.job.dto.JobDTO;
import alejandrocastrounqui.model.Job;

@Component
public class JobSerializer {

    ModelMapper modelMapper = new ModelMapper();

    public JobDTO modelToDTO(Job job) {
        JobDTO jobDTO = modelMapper.map(job, JobDTO.class);
        return jobDTO;
    }

    public Job dtoToModel(JobDTO jobDTO) {
        Job job = modelMapper.map(jobDTO, Job.class);
        return job;
    }

    public List<JobDTO> modelToDTO(List<Job> jobList) {
        List<JobDTO> result = new LinkedList<JobDTO>();
        for (Job job : jobList) {
            result.add(modelToDTO(job));
        }
        return result;
    }
}
