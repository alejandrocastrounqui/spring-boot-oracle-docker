package alejandrocastrounqui.api.job;

import org.springframework.stereotype.Repository;

import alejandrocastrounqui.common.BasicRepository;
import alejandrocastrounqui.model.Job;

@Repository("jobApiRepository")
public interface JobApiRepository extends BasicRepository<Job, String> {

}