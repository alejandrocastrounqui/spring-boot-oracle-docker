package alejandrocastrounqui.api.job;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import alejandrocastrounqui.api.job.dto.JobDTO;
import alejandrocastrounqui.model.Job;

@Transactional
@Service("JobApiService")
public class JobApiService {

    private static final Logger LOGGER = LoggerFactory.getLogger(JobApiService.class);

    @Autowired
    JobApiRepository jobApiRepository;

    @Autowired
    JobSerializer jobSerializer;

    public List<JobDTO> index() {
        LOGGER.info("retriving job index");
        List<Job> jobList = jobApiRepository.findAll();
        List<JobDTO> result = jobSerializer.modelToDTO(jobList);
        return result;
    }

}
