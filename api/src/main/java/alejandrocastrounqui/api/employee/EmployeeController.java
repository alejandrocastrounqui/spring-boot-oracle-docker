package alejandrocastrounqui.api.employee;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import alejandrocastrounqui.api.employee.dto.EmployeeCreatedDTO;
import alejandrocastrounqui.api.employee.dto.EmployeeDTO;
import alejandrocastrounqui.api.employee.dto.EmployeeIndexDTO;

@RestController
@RequestMapping("${rest.base_path}/employee")
public class EmployeeController {

    private static final Logger LOGGER = LoggerFactory.getLogger(EmployeeController.class);

    @Autowired
    EmployeeApiService employeeApiService;

    @ResponseBody
    @RequestMapping(value = "", method = RequestMethod.POST)
    public EmployeeCreatedDTO create(
        @RequestBody
        @Valid
        EmployeeDTO employeeDTO
            ) {
        //TODO check id=null
        LOGGER.info("creating new employee");
        return employeeApiService.create(employeeDTO);
    }

    @ResponseBody
    @RequestMapping(value = "", method = RequestMethod.PUT)
    public void update(@RequestBody EmployeeDTO employeeDTO) {
        //TODO check id!=null
        LOGGER.info("updating employee " + employeeDTO.getId());
        employeeApiService.update(employeeDTO);
    }

    @ResponseBody
    @RequestMapping(value = "{id}", method = RequestMethod.GET)
    public EmployeeDTO getById(@PathVariable("id") Integer id) {
        LOGGER.info("getting employee by id " + id);
        EmployeeDTO employeeDTO = employeeApiService.getById(id);
        return employeeDTO;
    }

    @ResponseBody
    @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    public EmployeeDTO delete(@PathVariable("id") Integer id) {
        LOGGER.info("deleting employee by id " + id);
        EmployeeDTO employeeDTO = employeeApiService.delete(id);
        return employeeDTO;
    }

    @ResponseBody
    @RequestMapping(value = "", method = RequestMethod.GET)
    public List<EmployeeIndexDTO> index(
        @RequestParam(name= "page", required = false)
        Integer page,
        @RequestParam(name= "size", required = false)
        Integer size,
        @RequestParam(name= "jobId", required = false)
        String jobId,
        @RequestParam(name= "managerId", required = false)
        Integer managerId,
        @RequestParam(name= "lastName", required = false)
        String lastName
            ) {
        LOGGER.info("retriving employees");
        List<EmployeeIndexDTO> employeeIndexDTOList = employeeApiService.index(page, size, jobId, managerId, lastName);
        return employeeIndexDTOList;
    }

}
