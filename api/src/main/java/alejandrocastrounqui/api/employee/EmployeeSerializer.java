package alejandrocastrounqui.api.employee;

import java.util.LinkedList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import alejandrocastrounqui.api.employee.dto.EmployeeCreatedDTO;
import alejandrocastrounqui.api.employee.dto.EmployeeDTO;
import alejandrocastrounqui.api.employee.dto.EmployeeIndexDTO;
import alejandrocastrounqui.api.employee.dto.EmployeeIndexManagerDTO;
import alejandrocastrounqui.model.Department;
import alejandrocastrounqui.model.Employee;
import alejandrocastrounqui.model.Job;

@Component
public class EmployeeSerializer {

    ModelMapper modelMapper = new ModelMapper();

    @Autowired
    EmployeeApiRepository employeeApiRepository;

    public EmployeeDTO modelToDTO(Employee employee) {
        EmployeeDTO employeeDTO = modelMapper.map(employee, EmployeeDTO.class);
        Job job = employee.getJob();
        if(job != null) {
            employeeDTO.setJob(job.getId());
        }
        Department department = employee.getDepartment();
        if(department != null) {
            employeeDTO.setDepartment(department.getId());
        }
        Employee manager = employee.getManager();
        if(manager != null) {
            employeeDTO.setManager(manager.getId());
        }
        return employeeDTO;
    }

    public EmployeeCreatedDTO modelToCreatedDTO(Employee employee) {
        EmployeeCreatedDTO employeeCreatedDTO = modelMapper.map(employee, EmployeeCreatedDTO.class);
        return employeeCreatedDTO;
    }

    public Employee dtoToModel(EmployeeDTO employeeDTO) {
        Employee employee = null;
        if(employeeDTO.getId() != null) {
            employee = employeeApiRepository.getOne(employeeDTO.getId());
            modelMapper.map(employeeDTO, employee);
        }
        else {
            employee = modelMapper.map(employeeDTO, Employee.class);
        }
        String jobId = employeeDTO.getJob();
        Job job = employeeApiRepository.getJob(jobId);
        employee.setJob(job);
        Integer departmentId = employeeDTO.getDepartment();
        Department department = employeeApiRepository.getDepartment(departmentId);
        employee.setDepartment(department);
        Integer managerId = employeeDTO.getManager();
        Employee manager = employeeApiRepository.getOne(managerId);
        employee.setManager(manager);
        return employee;
    }

    public List<EmployeeIndexDTO> modelToIndexDTO(List<Employee> employees) {
        List<EmployeeIndexDTO> result = new LinkedList<EmployeeIndexDTO>();
        for (Employee employee : employees) {
            EmployeeIndexDTO employeeIndexDTO = modelMapper.map(employee, EmployeeIndexDTO.class);
            Employee manager = employee.getManager();
            if(manager != null) {
                //manager must be mapped programmatically due to its recursive relationship
                EmployeeIndexManagerDTO indexManager = modelMapper.map(manager, EmployeeIndexManagerDTO.class);
                employeeIndexDTO.setManager(indexManager);
            }
            result.add(employeeIndexDTO);
        }
        return result;
    }

}
