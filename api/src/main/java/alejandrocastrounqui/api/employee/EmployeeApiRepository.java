package alejandrocastrounqui.api.employee;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import alejandrocastrounqui.common.BasicRepository;
import alejandrocastrounqui.model.Department;
import alejandrocastrounqui.model.Employee;
import alejandrocastrounqui.model.Job;

@Repository("employeeApiRepository")
public interface EmployeeApiRepository extends BasicRepository<Employee, Integer> {

    @Query("SELECT job " +
            "FROM Job job " +
            "WHERE job.id = :jobId")
    Job getJob(@Param("jobId") String jobId);

    @Query("SELECT department " +
            "FROM Department department " +
            "WHERE department.id = :departmentId")
    Department getDepartment(@Param("departmentId") Integer departmentId);

    @Query("SELECT employee " +
            "FROM Employee employee " +
            "LEFT JOIN employee.manager manager " +
            "LEFT JOIN employee.job job " +
            "WHERE ((:jobId IS NULL) " +
            "  OR (job.id = :jobId)) " +
            "AND ((:managerId IS NULL) " +
            "  OR (manager.id = :managerId)) " +
            "AND ((:lastName IS NULL) " +
            "  OR (employee.lastName LIKE CONCAT('%', :lastName, '%'))) ")
    List<Employee> filter(
        @Param("jobId") String jobId,
        @Param("managerId") Integer managerId,
        @Param("lastName") String lastName);


}