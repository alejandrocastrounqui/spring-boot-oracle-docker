package alejandrocastrounqui.api.employee;

import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import alejandrocastrounqui.api.employee.dto.EmployeeCreatedDTO;
import alejandrocastrounqui.api.employee.dto.EmployeeDTO;
import alejandrocastrounqui.api.employee.dto.EmployeeIndexDTO;
import alejandrocastrounqui.api.employee.extra.EmployeeHireDateComparator;
import alejandrocastrounqui.model.Employee;

@Transactional
@Service("employeeApiService")
public class EmployeeApiService {

    private static final Logger LOGGER = LoggerFactory.getLogger(EmployeeApiService.class);

    @Autowired
    EmployeeApiRepository employeeApiRepository;

    @Autowired
    EmployeeSerializer employeeSerializer;

    private EmployeeHireDateComparator employeeHireDateComparator = new EmployeeHireDateComparator();

    @Transactional
    public EmployeeCreatedDTO create(EmployeeDTO employeeDTO) {
        Employee employee = employeeSerializer.dtoToModel(employeeDTO);
        employeeApiRepository.save(employee);
        EmployeeCreatedDTO employeeCreatedDTO = employeeSerializer.modelToCreatedDTO(employee);
        return employeeCreatedDTO;
    }

    @Transactional
    public void update(EmployeeDTO employeeDTO) {
        Employee employee = employeeSerializer.dtoToModel(employeeDTO);
        employeeApiRepository.save(employee);
    }

    public EmployeeDTO getById(Integer id) {
        LOGGER.info("getting EmployeeDTO for id " + id);
        Employee employee = employeeApiRepository.getOne(id);
        if(employee == null) {
            throw new ResourceNotFoundException();
        }
        EmployeeDTO employeeDTO = employeeSerializer.modelToDTO(employee);
        return employeeDTO;
    }

    public EmployeeDTO delete(Integer id) {
        LOGGER.info("deleting employee for id " + id);
        Employee employee = employeeApiRepository.getOne(id);
        if(employee == null) {
            throw new ResourceNotFoundException();
        }
        EmployeeDTO employeeDTO = employeeSerializer.modelToDTO(employee);
        employeeApiRepository.delete(employee);
        return employeeDTO;
    }

    /**
     * page is base-0
     */
    public List<EmployeeIndexDTO> index(
        Integer page,
        Integer size,
        String jobId,
        Integer managerId,
        String lastName
            ) {
        if(page == null) {
            page = 0;
        }
        if(size == null) {
            size = 10;
        }
        Long employeesAmount = employeeApiRepository.count();
        LOGGER.info("current employee total amount: " + employeesAmount);
        List<Employee> employeeList = employeeApiRepository.filter(jobId, managerId, lastName);
        employeeList.sort(employeeHireDateComparator);
        int fromIndex = page * size;
        if(fromIndex >= employeeList.size()) {
            return Collections.emptyList();
        }
        int toIndex = page+1 * size;
        if(toIndex > employeeList.size()) {
            toIndex = employeeList.size();
        }
        List<Employee> pageItems = employeeList.subList(fromIndex, toIndex);
        return employeeSerializer.modelToIndexDTO(pageItems);
    }

}
