package alejandrocastrounqui.api.employee.dto;

import java.util.Date;
import java.util.List;


public class EmployeeIndexDTO {

    private Integer id;

    private String firstName;

    private String lastName;

    private String email;

    private String phoneNumber;

    private Date hireDate;

    private Double salary;

    private Double commisionPCT;

    private EmployeeIndexDepartmentDTO department;

    private EmployeeIndexManagerDTO manager;

    private EmployeeIndexJobDTO job;

    private List<EmployeeIndexJobHistoryItem> jobHistory;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Date getHireDate() {
        return hireDate;
    }

    public void setHireDate(Date hireDate) {
        this.hireDate = hireDate;
    }

    public Double getSalary() {
        return salary;
    }

    public void setSalary(Double salary) {
        this.salary = salary;
    }

    public Double getCommisionPCT() {
        return commisionPCT;
    }

    public void setCommisionPCT(Double commisionPCT) {
        this.commisionPCT = commisionPCT;
    }

    public EmployeeIndexDepartmentDTO getDepartment() {
        return department;
    }

    public void setDepartment(EmployeeIndexDepartmentDTO department) {
        this.department = department;
    }

    public EmployeeIndexJobDTO getJob() {
        return job;
    }

    public void setJob(EmployeeIndexJobDTO job) {
        this.job = job;
    }

    public EmployeeIndexManagerDTO getManager() {
        return manager;
    }

    public void setManager(EmployeeIndexManagerDTO manager) {
        this.manager = manager;
    }

    public List<EmployeeIndexJobHistoryItem> getJobHistory() {
        return jobHistory;
    }

    public void setJobHistory(List<EmployeeIndexJobHistoryItem> jobHistory) {
        this.jobHistory = jobHistory;
    }




}
