package alejandrocastrounqui.api.employee.dto;

public class EmployeeIndexLocationDTO {

    private Integer id;

    private String streetAddress;

    private String postalCode;

    private String city;

    private String stateProvince;

    private EmployeeIndexCountryDTO country;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStateProvince() {
        return stateProvince;
    }

    public void setStateProvince(String stateProvince) {
        this.stateProvince = stateProvince;
    }

    public EmployeeIndexCountryDTO getCountry() {
        return country;
    }

    public void setCountry(EmployeeIndexCountryDTO country) {
        this.country = country;
    }


}
