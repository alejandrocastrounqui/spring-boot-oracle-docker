package alejandrocastrounqui.api.employee.dto;

import java.util.Date;

public class EmployeeIndexJobHistoryItem {

    private EmployeeIndexDepartmentDTO department;

    private EmployeeIndexJobDTO job;

    private Date startDate;

    private Date endDate;

    public EmployeeIndexDepartmentDTO getDepartment() {
        return department;
    }

    public void setDepartment(EmployeeIndexDepartmentDTO department) {
        this.department = department;
    }

    public EmployeeIndexJobDTO getJob() {
        return job;
    }

    public void setJob(EmployeeIndexJobDTO job) {
        this.job = job;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }


}
