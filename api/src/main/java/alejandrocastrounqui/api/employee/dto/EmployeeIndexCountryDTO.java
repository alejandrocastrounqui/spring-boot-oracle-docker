package alejandrocastrounqui.api.employee.dto;

public class EmployeeIndexCountryDTO {

    private String id;

    private String name;

    private EmployeeIndexRegionDTO region;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public EmployeeIndexRegionDTO getRegion() {
        return region;
    }

    public void setRegion(EmployeeIndexRegionDTO region) {
        this.region = region;
    }


}
