package alejandrocastrounqui.api.employee.dto;

import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class EmployeeDTO {

    private Integer id;

    private String firstName;

    @NotBlank(message = "last-name-not-blank")
    private String lastName;

    @NotBlank(message = "email-not-blank")
    private String email;

    private String phoneNumber;

    @NotNull(message = "hire-date-not-null")
    private Date hireDate;

    @NotNull(message = "salary-not-null")
    private Double salary;

    @NotNull(message = "commision-pct-not-null")
    private Double commisionPCT;

    private Integer manager;

    private Integer department;

    @NotBlank(message = "job-not-blank")
    private String job;

    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getPhoneNumber() {
        return phoneNumber;
    }
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
    public Date getHireDate() {
        return hireDate;
    }
    public void setHireDate(Date hireDate) {
        this.hireDate = hireDate;
    }
    public Double getSalary() {
        return salary;
    }
    public void setSalary(Double salary) {
        this.salary = salary;
    }
    public Double getCommisionPCT() {
        return commisionPCT;
    }
    public void setCommisionPCT(Double commisionPCT) {
        this.commisionPCT = commisionPCT;
    }
    public Integer getManager() {
        return manager;
    }
    public void setManager(Integer manager) {
        this.manager = manager;
    }
    public Integer getDepartment() {
        return department;
    }
    public void setDepartment(Integer department) {
        this.department = department;
    }
    public String getJob() {
        return job;
    }
    public void setJob(String job) {
        this.job = job;
    }



}
