package alejandrocastrounqui.api.employee.dto;

public class EmployeeIndexDepartmentDTO {

    private Integer id;

    private String name;

    private EmployeeIndexLocationDTO location;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public EmployeeIndexLocationDTO getLocation() {
        return location;
    }

    public void setLocation(EmployeeIndexLocationDTO location) {
        this.location = location;
    }


}
