package alejandrocastrounqui.api.employee.extra;

import java.util.Comparator;
import java.util.Date;

import alejandrocastrounqui.model.Employee;

public class EmployeeHireDateComparator implements Comparator<Employee>{

    @Override
    public int compare(Employee left, Employee right) {
        Date leftHireDate = left.getHireDate();
        if(leftHireDate == null) {
            return -1;
        }
        return leftHireDate.compareTo(right.getHireDate());
    }

}
