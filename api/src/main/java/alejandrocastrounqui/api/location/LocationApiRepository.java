package alejandrocastrounqui.api.location;

import org.springframework.stereotype.Repository;

import alejandrocastrounqui.common.BasicRepository;
import alejandrocastrounqui.model.Location;

@Repository("locationApiRepository")
public interface LocationApiRepository extends BasicRepository<Location, String> {

}