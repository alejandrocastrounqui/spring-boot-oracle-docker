package alejandrocastrounqui.api.location;

import java.util.LinkedList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import alejandrocastrounqui.api.location.dto.LocationDTO;
import alejandrocastrounqui.model.Location;

@Component
public class LocationSerializer {

    ModelMapper modelMapper = new ModelMapper();

    public LocationDTO modelToDTO(Location location) {
        LocationDTO locationDTO = modelMapper.map(location, LocationDTO.class);
        return locationDTO;
    }

    public Location dtoToModel(LocationDTO locationDTO) {
        Location location = modelMapper.map(locationDTO, Location.class);
        return location;
    }

    public List<LocationDTO> modelToDTO(List<Location> locationList) {
        List<LocationDTO> result = new LinkedList<LocationDTO>();
        for (Location location : locationList) {
            result.add(modelToDTO(location));
        }
        return result;
    }
}
