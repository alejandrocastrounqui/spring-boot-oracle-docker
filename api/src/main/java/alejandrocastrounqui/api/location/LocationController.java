package alejandrocastrounqui.api.location;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import alejandrocastrounqui.api.location.dto.LocationDTO;

@RestController
@RequestMapping("${rest.base_path}/location")
public class LocationController {

    private static final Logger LOGGER = LoggerFactory.getLogger(LocationController.class);

    @Autowired
    LocationApiService locationApiService;

    @ResponseBody
    @RequestMapping(value = "", method = RequestMethod.GET)
    public List<LocationDTO> index() {
        LOGGER.info("retriving cuentasContables");
        List<LocationDTO> jopIndex = locationApiService.index();
        return jopIndex;
    }

}
