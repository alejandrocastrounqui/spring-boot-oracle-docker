package alejandrocastrounqui.api.location;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import alejandrocastrounqui.api.location.dto.LocationDTO;
import alejandrocastrounqui.model.Location;

@Transactional
@Service("LocationApiService")
public class LocationApiService {

    private static final Logger LOGGER = LoggerFactory.getLogger(LocationApiService.class);

    @Autowired
    LocationApiRepository locationApiRepository;

    @Autowired
    LocationSerializer locationSerializer;

    public List<LocationDTO> index() {
        LOGGER.info("retriving location index");
        List<Location> locationList = locationApiRepository.findAll();
        List<LocationDTO> result = locationSerializer.modelToDTO(locationList);
        return result;
    }

}
