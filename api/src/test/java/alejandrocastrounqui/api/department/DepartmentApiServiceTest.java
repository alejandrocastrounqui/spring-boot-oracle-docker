package alejandrocastrounqui.api.department;

import java.util.LinkedList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import alejandrocastrounqui.api.department.extra.InvalidContextException;
import alejandrocastrounqui.model.Employee;
import alejandrocastrounqui.model.Location;
import alejandrocastrounqui.service.DateAndTimeService;

@RunWith(SpringRunner.class)
public class DepartmentApiServiceTest {

    @TestConfiguration
    static class DepartmentApiServiceTestContextConfiguration {

        @Bean
        public DepartmentApiService departmentApiService() {
            return new DepartmentApiService();
        }

    }

    @Autowired
    private DepartmentApiService departmentApiService;

    @MockBean
    private DepartmentSerializer departmentSerializer;

    @MockBean
    private DateAndTimeService dateAndTimeService;

    @MockBean
    private DepartmentApiRepository departmentApiRepository;

    private Location location = new Location();

    @Test(expected = InvalidContextException.class)
    public void whenInvalidSalaryAverageIsGreaterThan1000AnIsDay14_thenDepartmentShouldThrowInvalidContextException() {
        //arrange
        Mockito.when(dateAndTimeService.dayOfMonth()).thenReturn(14);
        Employee employee1001 = new Employee();
        employee1001.setSalary(1001.0);
        List<Employee> employeeList = new LinkedList<Employee>();
        employeeList.add(employee1001);
        List<Employee> employeesByLocation = departmentApiRepository.getEmployeesByLocation(this.location);
        Mockito.when(employeesByLocation).thenReturn(employeeList);
        //act
        departmentApiService.validateAverageSalary(location);
        //assert @Test annotation
    }

    @Test(expected = Test.None.class)
    public void whenInvalidSalaryAverageIsGreaterThan1000AnIsDay15_thenDepartmentShouldNotThrowException() {
        //arrange
        Mockito.when(dateAndTimeService.dayOfMonth()).thenReturn(15);
        Employee employee1001 = new Employee();
        employee1001.setSalary(1001.0);
        List<Employee> employeeList = new LinkedList<Employee>();
        employeeList.add(employee1001);
        List<Employee> employeesByLocation = departmentApiRepository.getEmployeesByLocation(this.location);
        Mockito.when(employeesByLocation).thenReturn(employeeList);
        //act
        departmentApiService.validateAverageSalary(location);
        //assert @Test annotation
    }

    @Test(expected = InvalidContextException.class)
    public void whenInvalidSalaryAverageIsGreaterThan1500AnIsDay15_thenDepartmentShouldThrowInvalidContextException() {
        //arrange
        Mockito.when(dateAndTimeService.dayOfMonth()).thenReturn(15);
        Employee employee1501 = new Employee();
        employee1501.setSalary(1501.0);
        List<Employee> employeeList = new LinkedList<Employee>();
        employeeList.add(employee1501);
        List<Employee> employeesByLocation = departmentApiRepository.getEmployeesByLocation(this.location);
        Mockito.when(employeesByLocation).thenReturn(employeeList);
        //act
        departmentApiService.validateAverageSalary(location);
        //assert @Test annotation
    }

    @Test(expected = Test.None.class)
    public void whenInvalidSalaryAverageIsLessThan1000AnIsDay14_thenDepartmentShouldNotThrowException() {
        //arrange
        Mockito.when(dateAndTimeService.dayOfMonth()).thenReturn(14);
        Employee employee999 = new Employee();
        employee999.setSalary(999.0);
        List<Employee> employeeList = new LinkedList<Employee>();
        employeeList.add(employee999);
        List<Employee> employeesByLocation = departmentApiRepository.getEmployeesByLocation(this.location);
        Mockito.when(employeesByLocation).thenReturn(employeeList);
        //act
        departmentApiService.validateAverageSalary(location);
        //assert @Test annotation
    }

    @Test(expected = Test.None.class)
    public void whenInvalidSalaryAverageIsLessThan1000With2EmployeesAnIsDay14_thenDepartmentShouldNotThrowException() {
        //arrange
        Mockito.when(dateAndTimeService.dayOfMonth()).thenReturn(14);
        Employee employee1199 = new Employee();
        employee1199.setSalary(1199.0);
        Employee employee800 = new Employee();
        employee800.setSalary(800.0);
        List<Employee> employeeList = new LinkedList<Employee>();
        employeeList.add(employee1199);
        employeeList.add(employee800);
        List<Employee> employeesByLocation = departmentApiRepository.getEmployeesByLocation(this.location);
        Mockito.when(employeesByLocation).thenReturn(employeeList);
        //act
        departmentApiService.validateAverageSalary(location);
        //assert @Test annotation
    }

    @Test(expected = Test.None.class)
    public void whenInvalidSalaryAverageIsLessThan1000WithEmptyEmployeesAnIsDay14_thenDepartmentShouldNotThrowException() {
        //arrange
        Mockito.when(dateAndTimeService.dayOfMonth()).thenReturn(14);
        List<Employee> employeeList = new LinkedList<Employee>();
        List<Employee> employeesByLocation = departmentApiRepository.getEmployeesByLocation(this.location);
        Mockito.when(employeesByLocation).thenReturn(employeeList);
        //act
        departmentApiService.validateAverageSalary(location);
        //assert @Test annotation
    }

}
